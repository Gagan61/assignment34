﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{
    public partial class Form2 : Form
    {
        public Form2(double dis, int t, string from, string to)
        {
            InitializeComponent();
            double distance = dis;
            int type = t;
            string frmLoc = from;
            string tloc = to;
            double bf = 2.50, sv = 1.75;
            //Console.WriteLine("parameters " + distance + " " + type + " " + frmLoc + "  "+ tloc);
            if (type == 1)
            {
                label1.Text = "Ryde Pool Confirmed...!";
            }else if(type == 2)
            {
                label1.Text = "Ryde Direct Confirmed...!";
            }
            /*
             *  Base fare: $2.50
                Distance charge:  $0.81 / km travelled
                Service fees: $1.75

             */
            double dc = 0.81 * distance;
            label2.Text = "From: " + frmLoc;
            label3.Text = "To: " + tloc;
            label4.Text = "Base Fee: $" + bf;
            label5.Text = "Distance Charges: $" + dc;
            label6.Text = "Service Fee: $" + sv;
                //Console.WriteLine("Normal price : " + bf + " " + dc);
            if(type == 2) // if direct option is selected
            {
                bf = bf + (0.1 * bf);
                dc = dc + (0.15 * dc);
                //Console.WriteLine("increased price: " + bf + " " + dc);
            }
            double total = (bf + dc + sv);

            TimeSpan time = DateTime.Now.TimeOfDay;
            if ((time >= new TimeSpan(10, 0, 0)) && (time <= new TimeSpan(12, 0, 0)) ||  // peak time 10am - 12pm
                 (time >= new TimeSpan(16, 0, 0)) && (time <= new TimeSpan(18, 0, 0)) || // peak time 4pm - 6pm
                 (time >= new TimeSpan(20, 0, 0)) && (time <= new TimeSpan(21, 0, 0)))   // peak time 8pm - 9pm
            {
                total = total + (0.2 * total);
            }
            if (total < 5.50)  // if total amount is less than 5.50 then set minimum fare 5.50
            {
                total = 5.50;
            }
            
            label7.Text = "Total: $" + Math.Round(total, 2);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fromLoc = textBox1.Text;
            string toLoc = textBox2.Text;
            int type = 0;
            double dis = 0.0;
            bool isValid = true;
            if (fromLoc == "" || toLoc == "")
            {
                MessageBox.Show("Please Enter From & To Location.");
                isValid = false;
            }
            else if (fromLoc == "275 Yorkland Blvd" && toLoc == "CN Tower") 
            {
                dis = 22.9;
                //invalid = true;
            }else if (fromLoc == "Fairview Mall" && toLoc == "Tim Hortons")
            {
                dis = 1.2;
                //invalid = true;
            }
            else
            {
                MessageBox.Show("Please select valid From/To locations.");
                isValid = false;
            }


            if (radioButton1.Checked == false && radioButton2.Checked == false)
            {
                MessageBox.Show("Please select type Pool or Direct");
                isValid = false;
            }
            if(radioButton1.Checked == true )
            {
                type = 1;
            }
            if (radioButton2.Checked == true)
            {
                type = 2;
            }
           
            if (isValid)
            {
                Form2 f2 = new Form2(dis, type, fromLoc, toLoc);
                f2.ShowDialog();
            }
        }
    }
}
